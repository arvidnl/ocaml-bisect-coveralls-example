#!/bin/bash

set -e
set -x

if [ -z "$1" ]; then
    echo "Usage: $0 <bisect-coverage.json>"
    exit 1;
fi

#shellcheck disable=SC2001
CI_COMMIT_AUTHOR_NAME=$(echo "$CI_COMMIT_AUTHOR"  | sed 's/ <.*//')
#shellcheck disable=SC2001
CI_COMMIT_AUTHOR_EMAIL=$(echo "$CI_COMMIT_AUTHOR"  | sed 's/.*<\(.*\)>.*/\1/')

export RUN_AT=$CI_JOB_STARTED_AT
export SERVICE_NAME="gitlab-ci"
export SERVICE_NUMBER=$CI_PIPELINE_IID
export SERVICE_JOB_ID=$CI_JOB_ID
export PULL_REQUEST_ID
PULL_REQUEST_ID=$(echo "$CI_OPEN_MERGE_REQUESTS" | cut -d',' -f1)
export COMMIT_SHA=$CI_COMMIT_SHA
export AUTHOR_NAME=$CI_COMMIT_AUTHOR_NAME
export AUTHOR_EMAIL=$CI_COMMIT_AUTHOR_EMAIL
export COMMITER_MAIL=$CI_COMMIT_AUTHOR_NAME
export COMMITER_EMAIL=$CI_COMMIT_AUTHOR_EMAIL
export COMMITER_MESSAGE
COMMITER_MESSAGE=$(echo "$CI_COMMIT_MESSAGE" | sed -z 's@\n@\\n@g')
export BRANCH=$CI_COMMIT_BRANCH
export PARALLEL=${PARALLEL:-false}
export FLAG_NAME=${FLAG_NAME:-"test"}

./scripts/coverage_metadata.sh "$1"
