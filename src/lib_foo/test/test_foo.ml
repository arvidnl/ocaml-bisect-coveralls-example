let test_foo () = Foo.foo ()
let test_foo_aux () = Foo.foos_aux ()

let () =
  Alcotest.run
    "Bar"
    [("Test foo", [Alcotest.test_case "test foo" `Quick test_foo]);
     ("Test foo aux", [Alcotest.test_case "test foo aux" `Quick test_foo_aux])
    ]
