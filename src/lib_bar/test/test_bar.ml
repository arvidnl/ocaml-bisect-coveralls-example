let test_bar () =
  Bar.bar ()

let () =
  Alcotest.run
    "Bar"
    [("Test bar", [Alcotest.test_case "test bar" `Quick test_bar])]
