[![Coverage Status](https://coveralls.io/repos/gitlab/arvidnl/pbt-jsoo/badge.svg?branch=master)](https://coveralls.io/gitlab/arvidnl/pbt-jsoo?branch=master)

This is an example of an [Coveralls](https://coveralls.io) integration
for OCaml using [bisect_ppx](http://aantron.github.io/bisect_ppx/) in
GitLab CI.

# How it works

See:
 - The `dune` file, where `bisect_ppx` preprocessing is configured

 - The `.coveralls.yml` file, where the coveralls repository token is
   defined.

 - In the `Makefile`:
   - the job `test` (respectively `test-force`) runs the tests.
   - the job `coverage-raw.json` obtains the raw coverage data in a format
       understandable by Coveralsl
   - the job `coverage_glci.json` combines the latter with metadata
     available in the Gitlab CI instructure.
   - the job `report_coverage_glci` sends the report away to the
     Coveralls API.

 - The GitLab CI configuration in `.gitlab-ci` is based on [Gabriel
   Scherer's example GitLab CI configuration for OCaml
   projects](https://gitlab.com/gasche/gitlab-ocaml-ci-example).  My
   modifications can be found searching for `Coverage` and consists of
   calling `make report_coverage_glci` after running the tests.

# How it renders

See the results on [the coveralls
page](https://coveralls.io/gitlab/arvidnl/pbt-jsoo) for this project.

You can also see it in the gitlab pipelines for this for this project,
[like
here](https://gitlab.com/arvidnl/pbt-jsoo/-/pipelines/283370053/builds). You'll
notice two external jobs, "Coveralls" and "Coveralls - test" that link
to Coveralls. They also contain the total coverage percentage:

> ![](docs/coveralls-in-gitlab.png)
