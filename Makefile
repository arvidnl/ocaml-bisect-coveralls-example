BISECT_FILE:=$(shell pwd)/_coverage_output/
DUNE_OPTIONS:=

## Common

.PHONY: build setup_coverage
setup_coverage:
ifeq ($(COVERAGE),true)
	$(echo "true COVERAGE is $(COVERAGE)")
	$(eval DUNE_OPTIONS:=--instrument-with bisect_ppx)
	$(shell mkdir -p ${BISECT_FILE})
endif

## Building

.PHONY: build
build: setup_coverage
ifeq ($(COVERAGE),true)
	@echo "Building with coverage"
endif
	dune build ${DUNE_OPTIONS}

## Testing and coverage
.PHONY: test_unit_foo
test_unit_foo: setup_coverage
ifeq ($(COVERAGE),true)
	@echo "Unit testing (force) with coverage in: ${BISECT_FILE}$@/"
	@mkdir -p "${BISECT_FILE}$@/"
endif
	BISECT_FILE="${BISECT_FILE}$@/" dune runtest ${DUNE_OPTIONS} --force src/lib_foo
ifeq ($(COVERAGE),true)
	@echo "Coverage summary:"
	@bisect-ppx-report summary --per-file --coverage-path "${BISECT_FILE}$@/"
endif

.PHONY: test_unit_bar
test_unit_bar: setup_coverage
ifeq ($(COVERAGE),true)
	@echo "Unit testing (force) with coverage in: ${BISECT_FILE}$@/"
	@mkdir -p "${BISECT_FILE}$@/"
endif
	BISECT_FILE="${BISECT_FILE}$@/" dune runtest ${DUNE_OPTIONS} --force src/lib_bar
ifeq ($(COVERAGE),true)
	@echo "Coverage summary:"
	@bisect-ppx-report summary --per-file --coverage-path "${BISECT_FILE}$@/"
endif

.PHONY: test_integration
test_integration: setup_coverage
ifeq ($(COVERAGE),true)
	@echo "Integration testing with coverage in: ${BISECT_FILE}$@"
	@mkdir -p "${BISECT_FILE}$@/"
endif
	BISECT_FILE="${BISECT_FILE}$@/" dune exec ${DUNE_OPTIONS} src/bin_foobar/foobar.exe
ifeq ($(COVERAGE),true)
	@echo "Coverage summary:"
	@bisect-ppx-report summary --per-file --coverage-path "${BISECT_FILE}$@/"
endif

.PHONY: test
test: setup_coverage test_unit_foo test_unit_bar test_integration

.PHONY: test_opam
test_opam:
	opam pin add -w -k git -y --no-action ocaml-bisect-coveralls-example .
	opam install -y -j 4 --with-testocaml-bisect-coveralls-example

.PHONY: coverage-report-html
coverage-report-html:
	bisect-ppx-report html --coverage-path "${BISECT_FILE}"
	@echo Report in file://$(shell pwd)/_coverage/index.html

.PHONY: coverage-report-summary
coverage-report-summary:
	bisect-ppx-report summary --per-file --coverage-path "${BISECT_FILE}"

.PHONY: coverage-clean
coverage-clean:
	rm -rf _coverage_output/*

coverage-raw.json:
	bisect-ppx-report coveralls coverage-raw.json --coverage-path "${BISECT_FILE}"

coverage-local.json: coverage-raw.json
	./scripts/coverage_metadata_local.sh $< > $@

coverage-glci.json: coverage-raw.json
	./scripts/coverage_metadata_glci.sh $< > $@

report_coverage_local: coverage-local.json
	curl --location --request POST 'https://coveralls.io/api/v1/jobs' --form 'json_file=@${<}'

report_coverage_glci: coverage-glci.json
	curl --location --request POST 'https://coveralls.io/api/v1/jobs' --form 'json_file=@${<}'

## Misc
.PHONY: clean
clean:
	rm -f *.byte *.js *.cmi *.cmo coverage-raw.json coverage-local.json coverage-glci.json
